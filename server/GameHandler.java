package server;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import game2018.Player;
import messages.PlayerMove;
import messages.SocketMessage;
import messages.UpdateBoard;

public class GameHandler {
    
    private List<Player> players = new ArrayList<>();
    private List<ClientHandler> clients = new ArrayList<>();
    
    private String[] board = {
            "wwwwwwwwwwwwwwwwwwww",
            "w        ww        w",
            "w w  w  www w  w  ww",
            "w w  w   ww w  w  ww",
            "w  w               w",
            "w w w w w w w  w  ww",
            "w w     www w  w  ww",
            "w w     w w w  w  ww",
            "w   w w  w  w  w   w",
            "w     w  w  w  w   w",
            "w ww ww        w  ww",
            "w  w w    w    w  ww",
            "w        ww w  w  ww",
            "w         w w  w  ww",
            "w        w     w  ww",
            "w  w              ww",
            "w w  www  w w ww  ww",
            "w w      ww w     ww",
            "w   w   ww  w      w",
            "wwwwwwwwwwwwwwwwwwww"
    };

    public synchronized void addClient(ClientHandler client) {
        clients.remove(client);
        clients.add(client);
    }
    
    public synchronized int[] getNewBoardPosition() {
        int[] pos = new int[2];
        boolean found = false;
        Random rand = new Random();
        
        while (!found) {
            
            // X
            int randomNum = rand.nextInt(20);
            pos[0] = randomNum;
            
            // Y
            randomNum = rand.nextInt(20);
            pos[1] = randomNum;
            
            if (board[pos[1]].charAt(pos[0]) != 'w') {
                Player p = getPlayerAt(pos[0], pos[1]);
                if (p == null) {
                    found = true;
                }
            }
        }
        
        return pos;
    }
    
    public synchronized void sendBoard(ClientHandler clientHandler) throws Exception {
        SocketMessage msg = new SocketMessage("BOARD", board);
        clientHandler.getOut().writeObject(msg);
    }
    
    public synchronized void addPlayer(ClientHandler clientHandler, String name) throws Exception {
        String ip = (((InetSocketAddress) clientHandler.getClient().getRemoteSocketAddress()).getAddress()).toString().replace("/","");
        
        int[] pos = getNewBoardPosition();
        
        // Add something random to IP (make sure same computer can be on multiple times)
        int r = (int) (Math.random() * (1000 - 1)) + 1;
        ip += pos[0] + "-" + pos[1] + "-" + r;
        
        Player player = new Player(name, pos[0], pos[1], "up", ip);
        players.add(player);

        SocketMessage msg = new SocketMessage("CONNECT", player);
        sendToAll(msg);
        
        // Send CONNECT event to all clients except itself
        for (Player p : players) {
            if (!p.equals(player)) {
                msg = new SocketMessage("CONNECT", p);
                clientHandler.getOut().writeObject(msg);
            }
        }
        
    }

    public synchronized void sendToAll(SocketMessage msg) throws Exception {
        for (ClientHandler client : clients) {
            client.getOut().writeObject(msg);
            client.getOut().reset();
        }
    }

    /*
     * Player move
     */
    public synchronized Player getPlayerAt(int x, int y) {
        for (Player p : players) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }
    
    public synchronized Player getPlayer(Player p1) {
        for (Player p : players) {
            if (p.getIp().equals(p1.getIp())) {
                return p;
            }
        }
        return null;
    }

    public synchronized void movePlayer(PlayerMove move) throws Exception {

        boolean setGraphics = false;
            
        Player p = getPlayer(move.player);
        Player pAtSpot = null;
        
        p.setDirection(move.direction);
        
        int x = p.getXpos(), y = p.getYpos();
        
        if (board[y + move.delta_y].charAt(x + move.delta_x) == 'w') {
            p.addPoints(-1);
        } else {
            pAtSpot = getPlayerAt(x + move.delta_x, y + move.delta_y);
            if (pAtSpot != null) {
                p.addPoints(10);
                pAtSpot.addPoints(-10);
            } else {
                p.addPoints(1);
                
                x += move.delta_x;
                y += move.delta_y;
                
                p.setXpos(x);
                p.setYpos(y);
                setGraphics = true;
            }
        }
        
        move.player = p;
        
        UpdateBoard updateBoard = new UpdateBoard(players, move, setGraphics);
        SocketMessage msg = new SocketMessage("UPDATE", updateBoard);
        sendToAll(msg);
        
    }
    
}
