package server;
import java.net.*;

public class Server {
    
	/**
	 * @param args
	 */
    @SuppressWarnings("resource")
	public static void main(String[] args)throws Exception {
        ServerSocket socket = new ServerSocket(6789);
		System.out.println("Running server on port: 6789");
		
		GameHandler gameHandler = new GameHandler();
		
		while (true) {
			Socket client = socket.accept();

			ClientHandler clientHandler = new ClientHandler(client);
			gameHandler.addClient(clientHandler);
			
			(new ServerThread(clientHandler, gameHandler)).start();
		}
	}
    

}
