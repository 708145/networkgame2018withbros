package server;

import java.io.IOException;
import java.io.ObjectInputStream;

import messages.PlayerMove;
import messages.SocketMessage;

public class ServerThread extends Thread{
    private ClientHandler clientHandler;
    private GameHandler gameHandler;
	
	public ServerThread(ClientHandler clientHandler, GameHandler gameHandler) {
		this.clientHandler = clientHandler;
		this.gameHandler = gameHandler;
	}
	
	public void run() {

		try {
		    ObjectInputStream inFromClient = this.clientHandler.getIn();
			SocketMessage fromClient;
			
			while ((fromClient = (SocketMessage) inFromClient.readObject()) != null) {
			    
        			switch (fromClient.getType()) {
                        case "CONNECT":
                            String name = (String) fromClient.getObject();
                            gameHandler.sendBoard(this.clientHandler);
                            gameHandler.addPlayer(this.clientHandler, name);
                            break;
                			case "MOVE":
                			    PlayerMove move = (PlayerMove) fromClient.getObject();
                			    gameHandler.movePlayer(move);
                			    break;
            			    default:
            			        break;
        			}

            }
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
