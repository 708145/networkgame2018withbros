package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientHandler {
    
    private Socket client;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    
    public ClientHandler (Socket client) throws IOException {
        this.client = client;
        this.out = new ObjectOutputStream(client.getOutputStream());
        this.in = new ObjectInputStream(client.getInputStream());
    }
    
    public Socket getClient() {
        return client;
    }
    
    public ObjectOutputStream getOut() {
        return out;
    }
    
    public ObjectInputStream getIn() {
        return in;
    }

}
