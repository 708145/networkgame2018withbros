package messages;

import java.io.Serializable;
import java.util.List;

import game2018.Player;

public class UpdateBoard implements Serializable {
    
    private static final long serialVersionUID = -7436157382231946037L;
    
    private List<Player> players;
    private PlayerMove move;
    private boolean setGraphics;
    
    public UpdateBoard(List<Player> players, PlayerMove move, boolean setGraphics) {
        this.players = players;
        this.move = move;
        this.setGraphics = setGraphics;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public PlayerMove getMove() {
        return move;
    }
    
    public boolean shouldSetGraphics() {
        return this.setGraphics;
    }

}
