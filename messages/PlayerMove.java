package messages;

import java.io.Serializable;

import game2018.Player;

public class PlayerMove implements Serializable {
    
    private static final long serialVersionUID = 7437712730624696902L;
    
    public Player player;
    public int delta_x;
    public int delta_y;
    public String direction;
    
    public PlayerMove(Player player, int delta_x, int delta_y, String direction) {
        this.player = player;
        this.delta_x = delta_x;
        this.delta_y = delta_y;
        this.direction = direction;
    }

}
