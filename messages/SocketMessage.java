package messages;

import java.io.Serializable;

public class SocketMessage implements Serializable {

    private static final long serialVersionUID = -6127109669880218851L;
    private String type;
    private Object object;
    
    public SocketMessage(String type, Object object) {
        this.type = type;
        this.object = object;
    }

    public String getType() {
        return type;
    }

    public Object getObject() {
        return object;
    }
    
}
