package game2018;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import messages.PlayerMove;
import messages.SocketMessage;
import messages.UpdateBoard;

public class Main extends Application {
    
    private String socketServer = "10.24.17.219";
    
    private boolean initialized = false;

    public static final int    size         = 20;
    public static final int    scene_height = size * 20 + 100;
    public static final int    scene_width  = size * 20 + 200;

    public static Image        image_floor;
    public static Image        image_wall;
    public static Image        hero_right, hero_left, hero_up, hero_down;

    public static Player       me;
    public static List<Player> players      = new ArrayList<>();

    private Label[][]          fields;
    private TextArea           scoreList;

    //@formatter:off

	private  String[] board = {};
	
	private GridPane boardGrid;


	// -------------------------------------------
	// | Maze: (0,0)              | Score: (1,0) |
	// |-----------------------------------------|
	// | boardGrid (0,1)          | scorelist    |
	// |                          | (1,1)        |
	// -------------------------------------------

	//@formatter:on

	private Socket clientSocket;
    private ObjectOutputStream outToServer;
    private ObjectInputStream  inFromServer;

    class InThread extends Thread {

        ObjectInputStream in;

        public InThread(ObjectInputStream in) throws IOException {
            this.in = in;
        }

        @Override
        public void run() {
            try {

                SocketMessage inFromServer;
                while ((inFromServer = (SocketMessage) this.in.readObject()) != null) {

                    if (initialized) {
                            
                        switch (inFromServer.getType()) {
                            case "BOARD":
                                board = (String[]) inFromServer.getObject();
                                
                                Platform.runLater(() -> {
                                    try {
                                        drawBoard(board[0].length());
                                    } catch (Exception e) {
                                        Platform.exit();
                                    }
                                });
                                break;
                            case "CONNECT":
                                Player p = (Player) inFromServer.getObject();
                                players.add(p);
                                
                                if (me == null) {
                                    me = p;
                                }
                                
                                while (board.length == 0) {
                                    sleep(1000);
                                }
                                
                                Platform.runLater(() -> {
                                    fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(hero_up));
                                    scoreList.setText(getScoreList());
                                });
                                break;
                            case "UPDATE":
                                
                                UpdateBoard updateBoard = (UpdateBoard) inFromServer.getObject();
                                
                                players = updateBoard.getPlayers();
                                
                                for (Player p1 : players) {
                                    if (me != null && p1.getIp().equals(me.getIp())) {
                                        me = p1;
                                    }
                                }
                                
                                boolean setGraphics = updateBoard.shouldSetGraphics();
                                
                                Platform.runLater(() -> {
                                    // Update fields
                                    if (setGraphics) {
                                        PlayerMove move = updateBoard.getMove();
                                        int x = move.player.getXpos() - move.delta_x;
                                        int y = move.player.getYpos() - move.delta_y;
                                        
                                        fields[x][y].setGraphic(new ImageView(image_floor));
        
                                        x += move.delta_x;
                                        y += move.delta_y;
                                        
                                        if (move.player.getDirection().equals("right")) {
                                            fields[x][y].setGraphic(new ImageView(hero_right));
                                        }
        
                                        if (move.player.getDirection().equals("left")) {
                                            fields[x][y].setGraphic(new ImageView(hero_left));
                                        }
        
                                        if (move.player.getDirection().equals("up")) {
                                            fields[x][y].setGraphic(new ImageView(hero_up));
                                        }
        
                                        if (move.player.getDirection().equals("down")) {
                                            fields[x][y].setGraphic(new ImageView(hero_down));
                                        }
                                    }
                                    
                                    // Update scoreboard
                                    scoreList.setText(getScoreList());
                                });
                                
                                break;
                            default:
                                break;
                        }
                        
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startSocketClient() throws Exception {
        clientSocket = new Socket(socketServer, 6789);

        this.outToServer = new ObjectOutputStream(clientSocket.getOutputStream());
        this.inFromServer = new ObjectInputStream(clientSocket.getInputStream());

        (new InThread(this.inFromServer)).start();
    }

    private void addMeAsPlayer(String name) throws Exception {
        this.outToServer.writeObject(new SocketMessage("CONNECT", name));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        startSocketClient();

        try {
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(10, 10, 10, 10));

            Text nameLabel = new Text("Name:");
            nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            grid.add(nameLabel, 0, 0);
            
            TextField nameField = new TextField();
            nameField.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            grid.add(nameField, 0, 1);
            
            Button nameBtn = new Button("Start");
            grid.add(nameBtn, 0, 2);
            nameBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    String name = nameField.getText().trim();
                    if (name.length() != 0) {
                        try {
                            drawEverything(primaryStage, nameField.getText());
                        } catch (Exception e1) {
                            Platform.exit();
                        }
                    }
                }
            });
            
            Scene scene = new Scene(grid, scene_width, scene_height);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void drawEverything (Stage primaryStage, String name) throws Exception {
        initialized = true;
        addMeAsPlayer(name);
        
        try {
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(0, 10, 0, 10));

            Text mazeLabel = new Text("Maze:");
            mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            Text scoreLabel = new Text("Score:");
            scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            scoreList = new TextArea();

            boardGrid = new GridPane();

            image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
            image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size, false, false);

            hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
            hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
            hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
            hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);

            scoreList.setEditable(false);

            grid.add(mazeLabel, 0, 0);
            grid.add(scoreLabel, 1, 0);
            grid.add(boardGrid, 0, 1);
            grid.add(scoreList, 1, 1);

            Scene scene = new Scene(grid, scene_width, scene_height);
            primaryStage.setScene(scene);
            primaryStage.show();

            scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
                switch (event.getCode()) {
                case UP:
                    playerMoved(0, -1, "up");
                    break;
                case DOWN:
                    playerMoved(0, +1, "down");
                    break;
                case LEFT:
                    playerMoved(-1, 0, "left");
                    break;
                case RIGHT:
                    playerMoved(+1, 0, "right");
                    break;
                default:
                    break;
                }
            });

            scoreList.setText(getScoreList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    private void drawBoard(int size) throws Exception {
        fields = new Label[size][size];
        for (int j = 0; j < size; j++) {
            for (int i = 0; i < size; i++) {
                switch (board[j].charAt(i)) {
                case 'w':
                    fields[i][j] = new Label("", new ImageView(image_wall));
                    break;
                case ' ':
                    fields[i][j] = new Label("", new ImageView(image_floor));
                    break;
                default:
                    throw new Exception("Illegal field value: " + board[j].charAt(i));
                }
                boardGrid.add(fields[i][j], i, j);
            }
        }
    }

    public void playerMoved(int delta_x, int delta_y, String direction) {
        if (me == null) return;
        
        try {
            PlayerMove move = new PlayerMove(me, delta_x, delta_y, direction);

            SocketMessage msg = new SocketMessage("MOVE", move);
            this.outToServer.writeObject(msg);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    public String getScoreList() {
        StringBuffer b = new StringBuffer(100);
        for (Player p : players) {
            b.append(p + "\r\n");
        }
        return b.toString();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
