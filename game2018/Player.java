package game2018;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.UnknownHostException;

public class Player implements Serializable {
    private static final long serialVersionUID = -3277136807009569901L;

    String                    name;
    int                       xpos;
    int                       ypos;
    int                       point;
    String                    direction;
    String                    Player_IP;

    public Player(String name, int xpos, int ypos, String direction, String ip) throws UnknownHostException {
        this.name = name;
        this.xpos = xpos;
        this.ypos = ypos;
        this.direction = direction;
        this.point = 0;
        this.Player_IP = ip;
    }

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void addPoints(int p) {
        point += p;
    }

    public String getIp() {
        return Player_IP;
    }

    @Override
    public String toString() {
        return name + ":   " + point;
    }
}
